FROM python:buster
COPY src/ /app
RUN pip3 install -r /app/requirements.txt
WORKDIR /app/
ENTRYPOINT ["gunicorn", "--bind", "0.0.0.0:8001", "main:app"]
EXPOSE 8001
