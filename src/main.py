from flask import Flask, request, redirect
import geoip
import numpy as np

index_server = "cdn01.sto01.se.emea.thevoxel.net"

sites = [
    {
        "lat": 59.334591,
        "long": 18.063240,
        "host": "cdn01.sto01.se.emea.thevoxel.net"
    },
    {
        "lat": 50.110924,
        "long": 8.6821270,
        "host": "cdn01.fra01.de.emea.thevoxel.net"
    },
    {
        "lat": 33.753746,
        "long": -84.386330,
        "host": "cdn01.atl01.us.nasa.thevoxel.net"
    }
]

def get_closest_server(ip):
    if(ip is not None):
        lookup = geoip.geolite2.lookup(ip)
        coords = lookup.location
        xy = np.array([(i["lat"], i["long"]) for i in sites]).T
        distance_function = ((xy[0] - coords[0]) ** 2 + (xy[1] - coords[1]) ** 2) ** 0.5
        closest_index = np.argmin(distance_function)
        closest_server = sites[closest_index]["host"]
    else:
        closest_server = "cdn01.fra01.de.emea.thevoxel.net"
    return closest_server


app = Flask(__name__)

@app.route("/")
def main():
    ip = request.headers.get("X-Forwarded-For")
    closest_server = get_closest_server(ip)
    return {"closest_server": closest_server}


@app.route("/<path:cdnpath>")
def redirector(cdnpath):
    ip = request.headers.get("X-Forwarded-For")
    closest_server = get_closest_server(ip)
    return redirect(f"https://{closest_server}/{cdnpath}")
